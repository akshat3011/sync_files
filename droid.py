import os
import socket
import sys
import hashlib

block_size = 1024
# can use any other character to denote end_turn or special character
end_turn = 'end_turn'
sp_char = '&'


# function to send file take address, name and connection as parameters
def send_file(file_addr, filename, connection):
    print("Sending file " + filename)
    f = open(file_addr, 'rb')
    packet = f.read(block_size)

    while packet != '':
        print("."),
        connection.send(packet)
        packet = f.read(block_size)
    print('\n' + filename + " sent")
    f.close()
    connection.recv(block_size)


'''
 function to receive file takes ake address, name
 and connection, extra_info as parameters.
 extra_info is nothing but data of file received previously
 it saves need of one syncronization msg
 '''
def receive_file(my_dir, filename, connection, size, extra_info=''):
    print("receiving file " + filename)
    file_addr = os.path.join(my_dir, filename)
    f = open(file_addr, 'wb')
    f.write(extra_info)
    length = len(extra_info)
    while(length < size):
        data = connection.recv(block_size)
        f.write(data)
        length = length + len(data)
    connection.send("ok")
    print("received " + filename)
    f.close()


# function to copy all files and folder in a directory of droid A.
def droidA_directory(my_dir, droidA):
    # list of sub directories in given directory
    directory_list = []
    # adding end_turn at last of current files to shift turn to other party
    my_files = os.listdir(my_dir) + [end_turn]
    print('current directory: ' + my_dir)

    for filename in my_files:
        file_addr = os.path.join(my_dir, filename)

        if(os.path.isdir(file_addr)):
            # if file is a directory add it to directory list
            directory_list.append(filename)
        else:
            # send file + sp_char + size (knowing size of receiving file is
            # neccessary)
            if(filename == end_turn):
                droidA.send(filename + sp_char + "0")
                break

            my_size = os.stat(file_addr).st_size
            droidA.send(filename + sp_char + str(my_size))
            # receive reply from other droid
            data = droidA.recv(block_size)
            answer, size = data.split(sp_char)
            ''' recieve answer and size of file.
            If other party does nit have that file it will send no
            o/w it will send time and size'''

            # answer no means other droid does not contain this file so send
            if answer == 'no':
                send_file(file_addr, filename, droidA)
            else:
                # My(droidA) current file last modified time.
                my_file_time = os.stat(file_addr).st_mtime
                # droidB same file last modified time.
                time = answer

                # if size is same  demand for hash
                if (my_size == float(size)):
                    droidA.send("send_hash")

                    # receive droidB's same file hash
                    his_hash_value = droidA.recv(block_size)

                    # compute hash for own file.
                    f = open(file_addr, 'rb')
                    my_hash_value = hashlib.md5(f.read()).hexdigest()
                    f.close()

                    if(my_hash_value != his_hash_value):
                        '''if hash not same check for last modified time.
                        if droidA's file is last modifiend send it o/w request'''
                        if(my_file_time > float(time)):
                            send_file(file_addr, filename, droidA)
                        else:
                            droidA.send("request")
                    else:
                        # if hash matched inform other droid(droidB)
                        droidA.send("hash_matched")

                # if size is different take file which is recently modified
                elif(my_file_time < float(time)):
                    droidA.send("request")
                else:
                    send_file(file_addr, filename, droidA)

    while 1:
        '''this loop executes when droid A has finished sending
        all files of current directory to droid B and now is ready to recieve'''
        filename, size = droidA.recv(block_size).split(sp_char)
        # receive filename and size
        if(filename == end_turn):
            break
        # acknowledge to send him file necessary for syncronization
        droidA.send("ok")
        receive_file(my_dir, filename, droidA, float(size))

    # now recursively running droidA_directory function for sub folders in it
    # adding end_turn to pass turn to droidB
    for dir_name in directory_list + [end_turn]:
        # send directory name to droidB
        droidA.send(dir_name)
        if(dir_name != end_turn):
            # wait for droidB's acknowledgement
            reply = droidA.recv(block_size)
            droidA_directory(os.path.join(my_dir, dir_name),
                             droidA)    # run recursivily

    # now recursively running droidA_directory function for sub folders
    # in droidB_directory.
    while 1:
        dir_name = droidA.recv(block_size)
        if(dir_name == end_turn):
            # send acknowledgement
            droidA.send("ok")
            # if folder does not exists create it.
            break
        if not os.path.exists(os.path.join(my_dir, dir_name)):
            os.makedirs((os.path.join(my_dir, dir_name)))
        droidA_directory(os.path.join(my_dir, dir_name), droidA)


# this function is for directory of droid B
def droidB_directory(my_dir, droidB):
    # this list contains files that have been checked for by droid A
    # (no need to check for them again)
    checked_file = []
    # this list contains directories that have been checked for by droid A
    checked_dir = []
    directory_list = []
    # end turn to denote files for this folder are finished
    my_files = os.listdir(my_dir) + [end_turn]
    filename = ''
    print('current directory: ' + my_dir)
    while 1:
        # receive filename and size of file by droidA
        filename, size = droidB.recv(block_size).split(sp_char)
        if(filename == end_turn):
            break
        file_addr = os.path.join(my_dir, filename)

        # if droid B does not have file send no&0 o/w send 'time&size'
        if not(os.path.exists(file_addr)):
            droidB.send("no&0")
            receive_file(my_dir, filename, droidB, float(size))
            checked_file.append(filename)
        else:
            my_file_time = os.stat(file_addr).st_mtime
            my_size = os.stat(file_addr).st_size
            droidB.send(str(my_file_time) + sp_char + str(my_size))
            # receive info
            data = droidB.recv(block_size)
            if(data == "send_hash"):
                f = open(file_addr, 'rb')
                # send hash if droidA demands it
                hash_value = hashlib.md5(f.read()).hexdigest()
                f.close()
                droidB.send(hash_value)
                data = droidB.recv(block_size)
            if(data == "hash_matched"):
                # if hash matched add this to checked_file list
                checked_file.append(filename)

            elif(data != "request"):
                receive_file(my_dir, filename, droidB, float(size), data)
                # if data received was not request earlier data was sent needs
                # to written in file add it to exta_info
                checked_file.append(filename)

    # now droid B turn to send files
    for filename in my_files:
        if(filename not in checked_file):
            file_addr = os.path.join(my_dir, filename)
            if(os.path.isdir(file_addr)):
                directory_list.append(filename)
            else:
                if(filename == end_turn):
                    droidB.send(end_turn + sp_char + "0")
                else:
                    my_size = os.stat(file_addr).st_size
                    droidB.send(filename + sp_char + str(my_size))
                    droidB.recv(block_size)
                    send_file(file_addr, filename, droidB)

    # droidB receiving sub folders
    while(1):
        dir_name = droidB.recv(block_size)

        if(dir_name == end_turn):
            break
        droidB.send('ok')
        if not os.path.exists(os.path.join(my_dir, dir_name)):
            os.makedirs(os.path.join(my_dir, dir_name))
        checked_dir.append(dir_name)
        droidB_directory(os.path.join(my_dir, dir_name))
    # droidB sending sub folders
    for dir_name in directory_list + [end_turn]:
        if(dir_name == end_turn):
            droidB.send(dir_name)
            droidB.recv(block_size)
            break
        if(dir_name not in checked_dir):
            droidB.send(dir_name)
            droidB_directory(os.path.join(my_dir, dir_name), droidB)


def connect(host, port, my_dir):
    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    conn.connect((host, port))
    print("Sync Started")
    # on connect a droid becomes a droidA or droidB which depends on number
    # assigned by central server
    type = conn.recv(block_size)
    if (int(type) == 1):
        print("Role : Droid B")
        droidB_directory(my_dir, conn)
    else:
        print("Role : Droid A")
        droidA_directory(my_dir, conn)

    conn.close()
    print("Sync Completed Ty.")


server_ip = sys.argv[1]
server_port = sys.argv[2]
my_dir = sys.argv[3]

connect(server_ip, int(server_port), my_dir)
