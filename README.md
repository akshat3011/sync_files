## Syncing Files over Internet

**Submitted by:**
Akshat Choube, Harsh Yadav and Adrian McDonald Tariang

Networking Project, Semester VI, 2018

### Usage
First start the server side

`python server.py localhost 5555`

Next start the clients on either folder

`python droid.py localhost 5000 /home/directory_location`


## Documentation
server.py is a python file which runs this server on ip and port provided as command line arguments.

server.py exchanges these messages from one droid to another and helps in synchronization between them. One major task server does is assign role to droid connecting as droid A or droid B. droid A sends data first and droid B receives data first.

droid.py contains file synchronization functions for both droid roles(i.e droid A and droid B).

File is sent to another droid via Transmission Control Protocol (TCP).
droid.py has mainly 5 functions:

 - connect(host, port, my_dir) :- this function connects droid to server and my_dir is the directory that we want to sync.
 - send_file(file_addr,filename, connection): This function sends file to other droid and waits for an acknowledgement.
 - receive_file(my_dir,filename,connection,size,extra_info=''): This function receives file and stores it at my_dir. size denotes the size of file receiving. extra_info is the data received which must be written in the file. After receiving file receive_file function sends acknowledgement. This will be clear once you read algorithm.
 - droidA_directory(my_dir, droidA) is function which synchronizes files in my_dir path address passed as argument.
 - droidB_directory(my_dir, droidB) is function which synchronizes files in my_dir path address passed as argument for droidB.


 ## Algorithm
 
 - droidA searches for all files in my_dir.
 - If file is a directory add it to directory_list.
 - Else send filename + sp_char(special character) + size of file as a string to droid B. This spl_char is used to split filename and size. Filename should not contain this special character.
 - Droid A waits for reply from droid B.
 - Droid B receives filename and size. Checks if it has the file with filename as name. If file not found it sends “no” + sp_char +”0” otherwise it sends “size” + sp_char +”time”, where size is the size of file with same name as filename and time is last modified time for the same file.
- Droid A receives reply
    - If reply is “no” + sp_char + “0”. It means droid B does not have this file and so it
calls the send_file function.
    - Droid B receives the file and adds file in checked_list
    - If reply is not ‘no’ then it means droid B has file with same name, so extract size and last modified time of file.
    - If size is same as size of file which droid A has, droid A demands for the file hash.
        - Droid B receives the request for hash and sends hash of file.
        - A compares hashes. if they match, file contents are the same. So no need to send file to B.
        - Else if hashes are not same implies file contents are different and then droid A compares last modified time of both file. If A has recent file then A sends it otherwise A requests for it.
        - If A requests for a file, droid B adds it in request list o/w receives the file and adds the file in checked_list. Data received here is passed as an extra_info argument to receive function.
    - If file sizes are different.-
        - A compares last modified time of both file. If A has recent file then A sends it otherwise A requests for it.
        - If A requests for it droid B adds it in request list o/w receives file and adds file in checked_list.
 - After droid A has finished sending files it informs droid B by sending end_turn
 - After receiving end_turn. B starts sending files to A. B sends filename + sp_char + size.
 - B sends files which are not in checked_list.
 - A receives filename and size; and then sends an acknowledgement “ok” to droid
 - B which is necessary for synchronization.
 - Then droid A receives, and droid B sends using send_file function.
 - After droid B completes sending files, it sends end_turn to inform droid A.
 - Now it’s time for Droid A to recursively call the droidA_directory() function for subdirectories in directory list.
 - Droid A sends directory name to droid B and waits for response of droid B
 - Droid B receives directory’s name and sends “ok” as an acknowledgement, then adds the directory in the checked directory list, If directory does not exist create it and run droidB_directory function for that directory.
 - Droid A receives acknowledgement and recursively call droidA_directory for the subdirectory.
 - After droid A has finished all the subdirectories it sends end_turn, then droid B, in a similar fashion synchronizes its subdirectories with A.
