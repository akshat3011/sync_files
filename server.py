
import socket
import select
import sys
from thread import *

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# takes ip address and port from terminal
ip_address = str(sys.argv[1])
port = int(sys.argv[2])
server.bind((ip_address, port))
server.listen(5)
block_size = 1024

# list of droids currently connected to server
list_of_droids = []


def clientthread(conn, addr):
    # On first connection with a droid inform him whether he will be
    # acting a droidA or droidB
    if(len(list_of_droids) == 1):
        # for DroidA
        conn.send("1")
    else:
        # for DroidB
        conn.send("0")

    while True:
        try:
            message = conn.recv(block_size)
            if message:
                """prints the message and address of the
                user who just sent the message on the server
                terminal"""
                print "<" + addr[0] + "> " + message

                if(list_of_droids > 1):
                    forward(message, conn)

            else:
                """message may have no content if the connection
                is broken, in this case we remove the connection"""
                remove(conn)

        except:
            continue


"""This function forwards messages of droidA to droidB """
def forward(message, connection):
    for clients in list_of_droids:
        if clients != connection:
            try:
                clients.send(message)
            except:
                clients.close()
                # if the link is broken, we remove the client
                remove(clients)


"""The following function simply removes the object
from the list that was created at the beginning of
the program"""
def remove(connection):
    if connection in list_of_droids:
        list_of_droids.remove(connection)


print("Hello this is sync server")

while True:
    conn, addr = server.accept()
    conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    list_of_droids.append(conn)
    # prints the address of the user that just connected
    print addr[0] + " connected"
    # start thread for every connection
    start_new_thread(clientthread, (conn, addr))


conn.close()
server.close()
